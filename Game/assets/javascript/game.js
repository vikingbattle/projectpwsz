// ---------------------------------------------------------------------------------
// Character Object & Statistics
// ---------------------------------------------------------------------------------
checkCookie();
function checkCookie() {
  var id=getCookie("id");
  var user=getCookie("login");

  if (user != "") {
  	  } else {
       window.location.href = "../Frontend";
  }

}
function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

points=getCookie("points");
points = parseInt(points);

function characterStats(name, hp, ap) {
	this.name = name;				//Name
	this.hp = hp;					//Health Points
	this.ap = ap;					//Attack Points [initial] (will be updated every round for hero)
}
var user=getCookie("login");

var eliwood = new characterStats(
	'Eliwood',
	(47*(points/10)).toFixed(1),
	(6*(points/3)).toFixed(1),
);

var lyn = new characterStats(
	'Lyn',
	(47*(points/10)).toFixed(1),
	(6*(points/3)).toFixed(1),
);

var hector = new characterStats(
	'Hector',
	(47*(points/10)).toFixed(1),
	(6*(points/3)).toFixed(1),
);

var marcus = new characterStats(
	'Marcus',
	(47*(points/10)).toFixed(1),
	(6*(points/3)).toFixed(1),
);


// ---------------------------------------------------------------------------------
// Variables
// ---------------------------------------------------------------------------------

var hero = "";
var enemy = "";
var characters = ['eliwood', 'lyn', 'hector', 'marcus'];

// ---------------------------------------------------------------------------------
// Functions
// ---------------------------------------------------------------------------------

//Function to display all characters
function displayCharacters() {
	$('#message').html('<h2>Wybierz swoją postać</h2>');

	for (var i = 0; i < characters.length; i++) {
		var chara = characters[i];
		var html = '<div class="character ' + chara + '"><img class="character-img ' + chara + '" src="assets/images/profile/' + chara + '.gif" alt="' + this[chara].name + '" value="' + chara + '"></div>';
		$('#character-portraits').append(html);
	}

	hero = getCookie("hero");
	console.log(hero);
	console.log("Hero: " + window[hero].name);

	//Character portrait fades out and is removed after .5 seconds
	$('.character .'+hero).attr('class','animated fadeOut character ' + hero);
	setTimeout(function() {
		$('.character .'+hero).remove();
	},500);

	//Update message
	$('#message').html('<h2>Wybierz przeciwnika lub <a href="/dawid/Multiplayer" style="color: #fff;">zagraj z innymi graczami</a></h2>');

	//Add hero and hero stats to arena
	$('#arena').append('<img id="hero" src="assets/images/battle/' + hero + '.gif">');
	$('#hero').attr('class','animated fadeInRight');
	$('#hero-name').html(window[hero].name);
	$('#hero-stats').html('100<br><div id="hero-ap">' + window[hero].ap + '</div>');
	$('#hero-hp').html(window[hero].hp);
	$('#hero-name, #hero-stats, #hero-hp').attr('class','animated fadeIn');

	//Remove hero stats animation classes after 1 second
	setTimeout(function() {
		$('#hero-name, #hero-stats, #hero-hp').attr('class','');
	},1000);

	//Remove hero from charactesr array
	charaIndex = characters.indexOf(hero);
	characters.splice(charaIndex,1);
}

//Function to pick a hero and enemy
function pickCharacters() {
	//When a character image is clicked
	$('.character-img').on('click', function() {
		//If no enemy has been chosen
		if (enemy === "") {
			//Update enemy variable with character name
			enemy = $(this).attr('value');
			console.log("Enemy: " + window[enemy].name);

			//Character portrait fades out and is removed after .5 seconds
			$('.character .' + enemy).attr('class','animated fadeOut character ' + enemy);
			setTimeout(function() {
				$('.character .' + enemy).remove();
			},500);

			//Add enemy and enemy stats to arena
			$('#arena').append('<img id="enemy" src="assets/images/battle/enemy/' + enemy + '.gif">');
			$('#enemy').attr('class','animated fadeInLeft');
			$('#enemy-name').html(window[enemy].name);
			$('#enemy-stats').html('100<br>' + window[enemy].ap);
			$('#enemy-hp').html(window[enemy].hp);
			$('#enemy-name, #enemy-stats, #enemy-hp').attr('class','animated fadeIn');

			//Remove enemy stats animation classes after 1 second
			setTimeout(function() {
				$('#enemy-name, #enemy-stats, #enemy-hp').attr('class','');
			},1000);

			//Remove enemy from charactesr array
			charaIndex = characters.indexOf(enemy);
			characters.splice(charaIndex,1);
		}
	});
}

//Increase Hero AP every round and update display
function increaseHeroAP() {
	window[hero].ap = parseInt(window[hero].ap);
	$('#hero-ap').html(window[hero].ap);
	$('#hero-ap').attr('class','animated rubberBand');

	//Reset herp HP animation classes after 1 second
	setTimeout(function() {
		$('#hero-ap').attr('class','');
	},1000);
}

//Reduce HP after being attacked
function decreaseHP1(attacker,defender) {

	var chance;
	chance = (Math.floor(Math.random()*10+1));
	if( (chance<=8) && (chance>=2))
	{
	var rand_atack = (Math.floor(Math.random()*(window[attacker].ap)+1));
	window[defender].hp = window[defender].hp - (rand_atack+0);
	}
	else
	{
	var atack = 0
	window[defender].hp = window[defender].hp - atack;
	} chance=0;
}
function decreaseHP2(attacker,defender) {

	var chance;
	chance = (Math.floor(Math.random()*10+1));
	if( (chance<=7) && (chance>=3))
	{
	var rand_atack = (Math.floor(Math.random()*(window[attacker].ap)+1));
	window[defender].hp = window[defender].hp - (rand_atack+2);
	}
	else
	{
	var atack = 0
	window[defender].hp = window[defender].hp - atack;
	} chance=0;
}
function decreaseHP3(attacker,defender) {

	var chance;
	chance = (Math.floor(Math.random()*10+1));
	if( (chance<=6) && (chance>=4))
	{
	var rand_atack = (Math.floor(Math.random()*(window[attacker].ap)+1));
	window[defender].hp = window[defender].hp - (rand_atack+6);
	}
	else
	{
	var atack = 0
	window[defender].hp = window[defender].hp - atack;
	} chance=0;
}



//Time delays for characters
function attackTimeout(attacker) {
	//If 'hide animation' is not checked, return times based on animation gifs
		if (attacker == "eliwood") {
			return 3000;
		}
		else if (attacker == "lyn") {
			return 3800;
		}
		else if (attacker == "hector") {
			return 6000;
		}
		else if (attacker == "marcus") {
			return 4000;
		}
}

//End battle function: If hero is defeated, end game. If enemy is defeated, pick another enemy
//unless everyone is defeated. Parameter must be passed as string.
function endBattle() {
	//If hero is defeated
	if (window[hero].hp <= 0) {
		//Fade out hero + stats
		$('#hero').attr('class','animated fadeOutRight');
		$('#hero-name, #hero-stats, #hero-hp').attr('class','animated fadeOut');

		setTimeout(function() {
			$('#message').html('<h2 class="red">Przegrałeś! Nie otrzymałeś punktów po walce.</h2>');
			$('.arena-area').append('<button type="button" id="reset" class="btn btn-primary" onclick="location.reload()">Zagraj jeszcze raz?</button>');
		},1000);
	}

	//If enemy is defeated
	else if (window[enemy].hp <= 0) {

		console.log('You win!');
		//Fade out enemy
		$('#enemy').attr('class','animated fadeOutLeft');
		$('#enemy-name, #enemy-stats, #enemy-hp').attr('class','animated fadeOut');

		//Remove enemy image and reset enemy stats animation classes after 1 second
		setTimeout(function() {
			$('#enemy').remove();
			$('#enemy-name, #enemy-stats, #enemy-hp').attr('class','');
			$('#enemy-name, #enemy-stats, #enemy-hp').html('');

			//Increase hero AP after .5 seconds
			setTimeout(function () {
				//If everyone is defeated
				$('#message').html('<h2 class="green">You Win!</h2>');
				$('.arena-area').append('<button type="button" id="reset" class="btn btn-primary" onclick="location.reload()">Jeszcze raz?</button>');
				sendPoints();
			},500);

		},1000);
	}
}

function attack() {
	$('.arena-area').append('<br><button type="button" id="attack1" class="btn btn-danger">Szybki Atak</button>');
	$('.arena-area').append('<br><button type="button" id="attack2" class="btn btn-danger">Normalny Atak</button>');
	$('.arena-area').append('<br><button type="button" id="attack3" class="btn btn-danger">Mocny Atak</button>');
	console.log("Attack button initialized");

	//When Attack1 button is clicked    111
	$('#attack1').on('click', function() {
		button = document.getElementById('attack1').setAttribute("disabled", "");
		button = document.getElementById('attack2').setAttribute("disabled", "");
		button = document.getElementById('attack3').setAttribute("disabled", "");
		//If there's no enemy selected
		if (enemy == "" || window[hero].hp <= 0) {
			alert("Nie wybrałeś przeciwnika!");
			button = document.getElementById('attack1').removeAttribute("disabled", "");
			button = document.getElementById('attack2').removeAttribute("disabled", "");
			button = document.getElementById('attack3').removeAttribute("disabled", "");
		}

		//Otherwise run attack animations
		else {
			//If 'hide animation' is not checked, run animation gifs
			
				$('#hero').attr('src','assets/images/battle/' + hero + '-attack.gif');
				$('#hero').attr('class','attacker');
				$('#enemy').attr('class','defender');
			

			//Decrease enemy HP
			decreaseHP1(hero,enemy);

			//Enemy attacks after hero finishes attacking
			setTimeout(function() {
				//If 'hide animation' is not checked, run animation gifs
				
					$('#enemy').attr('src','assets/images/battle/enemy/' + enemy + '-attack.gif');
					$('#hero').attr('class','defender');
					$('#enemy').attr('class','attacker');
				

				//Decrease hero HP
				decreaseHP1(enemy,hero);

				//Update hero and enemy HPs after enemy finishes attacking
				setTimeout(function () {
					if (window[hero].hp <= 0) {
						$('#hero-hp').html('0');
					}
					else {
						$('#hero-hp').html(window[hero].hp);
					}
					$('#hero-hp').attr('class','animated bounce');

					if (window[enemy].hp <= 0) {
						$('#enemy-hp').html('0');
					}
					else {
						$('#enemy-hp').html(window[enemy].hp);
					}
					$('#enemy-hp').attr('class','animated bounce');

					//Rest HP animation classes and increase hero HP after .5 seconds
					setTimeout(function() {
						$('#hero-hp').attr('class','');
						$('#enemy-hp').attr('class','');
						increaseHeroAP();

						//If either enemy or hero is defeated, end battle
						if (window[enemy].hp <= 0 || window[hero].hp <= 0) {
							button = document.getElementById('attack1').setAttribute("disabled", "");
							endBattle();
						}
						button = document.getElementById('attack1').removeAttribute("disabled", "");
						button = document.getElementById('attack2').removeAttribute("disabled", "");
						button = document.getElementById('attack3').removeAttribute("disabled", "");
					},1000);

				},attackTimeout(enemy));

			},attackTimeout(hero));
		}
	});

	//When Attack2 button is clicked    222
	$('#attack2').on('click', function() {
		button = document.getElementById('attack1').setAttribute("disabled", "");
		button = document.getElementById('attack2').setAttribute("disabled", "");
		button = document.getElementById('attack3').setAttribute("disabled", "");
		//If there's no enemy selected
		if (enemy == "" || window[hero].hp <= 0) {
			alert("Nie wybrałeś przeciwnika!");
			button = document.getElementById('attack1').removeAttribute("disabled", "");
			button = document.getElementById('attack2').removeAttribute("disabled", "");
			button = document.getElementById('attack3').removeAttribute("disabled", "");
		}

		//Otherwise run attack animations
		else {
			//If 'hide animation' is not checked, run animation gifs
			
				$('#hero').attr('src','assets/images/battle/' + hero + '-attack.gif');
				$('#hero').attr('class','attacker');
				$('#enemy').attr('class','defender');
			

			//Decrease enemy HP
			decreaseHP2(hero,enemy);

			//Enemy attacks after hero finishes attacking
			setTimeout(function() {
				//If 'hide animation' is not checked, run animation gifs
				
					$('#enemy').attr('src','assets/images/battle/enemy/' + enemy + '-attack.gif');
					$('#hero').attr('class','defender');
					$('#enemy').attr('class','attacker');
				

				//Decrease hero HP
				decreaseHP2(enemy,hero);

				//Update hero and enemy HPs after enemy finishes attacking
				setTimeout(function () {
					if (window[hero].hp <= 0) {
						$('#hero-hp').html('0');
					}
					else {
						$('#hero-hp').html(window[hero].hp);
					}
					$('#hero-hp').attr('class','animated bounce');

					if (window[enemy].hp <= 0) {
						$('#enemy-hp').html('0');
					}
					else {
						$('#enemy-hp').html(window[enemy].hp);
					}
					$('#enemy-hp').attr('class','animated bounce');

					//Rest HP animation classes and increase hero HP after .5 seconds
					setTimeout(function() {
						$('#hero-hp').attr('class','');
						$('#enemy-hp').attr('class','');
						increaseHeroAP();

						//If either enemy or hero is defeated, end battle
						if (window[enemy].hp <= 0 || window[hero].hp <= 0) {
							button = document.getElementById('attack2').setAttribute("disabled", "");
							endBattle();
						}
						button = document.getElementById('attack1').removeAttribute("disabled", "");
						button = document.getElementById('attack2').removeAttribute("disabled", "");
						button = document.getElementById('attack3').removeAttribute("disabled", "");
					},1000);

				},attackTimeout(enemy));

			},attackTimeout(hero));
		}
	});

	//When Attack3 button is clicked    333
	$('#attack3').on('click', function() {
		button = document.getElementById('attack1').setAttribute("disabled", "");
		button = document.getElementById('attack2').setAttribute("disabled", "");
		button = document.getElementById('attack3').setAttribute("disabled", "");
		//If there's no enemy selected
		if (enemy == "" || window[hero].hp <= 0) {
			alert("Nie wybrałeś przeciwnika!");
			button = document.getElementById('attack1').removeAttribute("disabled", "");
			button = document.getElementById('attack2').removeAttribute("disabled", "");
			button = document.getElementById('attack3').removeAttribute("disabled", "");
		}

		//Otherwise run attack animations
		else {
			//If 'hide animation' is not checked, run animation gifs
			
				$('#hero').attr('src','assets/images/battle/' + hero + '-attack.gif');
				$('#hero').attr('class','attacker');
				$('#enemy').attr('class','defender');
			

			//Decrease enemy HP
			decreaseHP3(hero,enemy);

			//Enemy attacks after hero finishes attacking
			setTimeout(function() {
				//If 'hide animation' is not checked, run animation gifs
				
					$('#enemy').attr('src','assets/images/battle/enemy/' + enemy + '-attack.gif');
					$('#hero').attr('class','defender');
					$('#enemy').attr('class','attacker');
				

				//Decrease hero HP
				decreaseHP3(enemy,hero);

				//Update hero and enemy HPs after enemy finishes attacking
				setTimeout(function () {
					if (window[hero].hp <= 0) {
						$('#hero-hp').html('0');
					}
					else {
						$('#hero-hp').html(window[hero].hp);
					}
					$('#hero-hp').attr('class','animated bounce');

					if (window[enemy].hp <= 0) {
						$('#enemy-hp').html('0');
					}
					else {
						$('#enemy-hp').html(window[enemy].hp);
					}
					$('#enemy-hp').attr('class','animated bounce');

					//Rest HP animation classes and increase hero HP after .5 seconds
					setTimeout(function() {
						$('#hero-hp').attr('class','');
						$('#enemy-hp').attr('class','');
						increaseHeroAP();

						//If either enemy or hero is defeated, end battle
						if (window[enemy].hp <= 0 || window[hero].hp <= 0) {
							button = document.getElementById('attack3').setAttribute("disabled", "");
							endBattle();
						}
						button = document.getElementById('attack1').removeAttribute("disabled", "");
						button = document.getElementById('attack2').removeAttribute("disabled", "");
						button = document.getElementById('attack3').removeAttribute("disabled", "");
					},1000);

				},attackTimeout(enemy));

			},attackTimeout(hero));
		}
	});
}

function sendPoints() {
	id = getCookie("id");
	points = 10*(getCookie("points")/10);
    $.ajax({  
         type: "post", 
         url:"https://projektdawidkamil.przedprojekt.com/dawid/Backend/panel/players/add_points", 
         data: {id:id, points:points}, 
         cache: false, 
    }); 
}
// ---------------------------------------------------------------------------------
// JS to run on Window Load
// ---------------------------------------------------------------------------------

window.onload = function() {
	displayCharacters();
	pickCharacters();
	attack();
};

function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  var expires = "expires="+ d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
function logout() {
    setCookie('id', 0, 0);
    setCookie('login', 0, 0);
    setCookie('hero', 0, 0);
    setCookie('points', 0, 0);
	window.location.href = "../Frontend";
}