<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Active extends CI_Controller {
	public function index() {
		$insert['auth'] = 1;
		$insert['secret_key'] = '';
		if($this->apirest_m->activePlayer('players', $insert, $_GET['secret_key'])){
			$response = array('status' => 'OK');
			$this->output
			        ->set_status_header(200)
			        ->set_content_type('application/json', 'utf-8')
			        ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			        ->_display();
		redirect('https://projektdawidkamil.przedprojekt.com/dawid/Frontend/');
			exit;
		} else {
				$response = array('status' => 'Something goes wrong');
				$this->output
				        ->set_status_header(404)
				        ->set_content_type('application/json', 'utf-8')
				        ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
				        ->_display();
				exit;
		}
	}
}