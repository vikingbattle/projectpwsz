<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Players extends CI_Controller {

	public function index() {
		if(checkAccess($access_group = ['administrator', 'redaktor'], $_SESSION['rola'])) {
			if (!$this->db->table_exists($this->uri->segment(2))){
				$this->base_m->create_table($this->uri->segment(2));
			}
            // DEFAULT DATA
			$data = loadDefaultData();

			$data['rows'] = $this->back_m->get_all($this->uri->segment(2));
			echo loadSubViewsBack($this->uri->segment(2), 'index', $data);
		} else {
			redirect('panel');
		}
	}

	public function form($type, $id = '') {
		if(checkAccess($access_group = ['administrator', 'redaktor'], $_SESSION['rola'])) {
            // DEFAULT DATA
			$data = loadDefaultData();

            if($id != '') {
			    $data['value'] = $this->back_m->get_one($this->uri->segment(2), $id);
			    $data['ranking'] = $this->back_m->from_ranking('ranking', $id);
            }
			echo loadSubViewsBack($this->uri->segment(2), 'form', $data);
		} else {
			redirect('panel');
		}
	} 

	public function action($type, $table, $id = '') {
		if(checkAccess($access_group = ['administrator', 'redaktor'], $_SESSION['rola'])) {
			
			foreach ($_POST as $key => $value) {

				if (!$this->db->field_exists($key, $table)) {
					$this->base_m->create_column($table, $key);
				}
				$insert[$key] = $value; 

            }

			logs_message('Dane użytkownika ' . $insert['name'] . ' zostały zmienione przez ' . $_SESSION['name'], $_SERVER['HTTP_USER_AGENT'], $_SERVER['REMOTE_ADDR']);
			$this->back_m->update($table, $insert, $id);
			$this->session->set_flashdata('flashdata', 'Rekord został zaktualizowany!');   
            
			redirect('panel/'.$table);
		} else {
			redirect('panel');
		}
    }

	public function get_api() {
		$data['rows'] = $this->back_m->get_all($this->uri->segment(2));
		$this->output->set_content_type('application/json');
		$this->output->set_status_header(200);
		$this->output->set_output(json_encode($data['rows']));
	}

	public function get_players() {
		$data['rows'] = $this->back_m->get_all('players');
		$array = array();
		foreach ($data['rows'] as $k => $v) {
			$player['name'] = $v->name;
			$player['hero'] = $v->hero;
			$player['points'] = $this->back_m->get_player_points('ranking', $v->id)->points;
			array_push($array, $player);
		}
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($array));
	}

	public function add_player() {

		header("Access-Control-Allow-Origin: *");
		$_POST = $this->security->xss_clean($_POST);
		$this->form_validation->set_rules('name', 'Login', 'trim|required|min_length[2]|is_unique[players.name]');
		$this->form_validation->set_rules('email', 'E-mail', 'trim|valid_email|required|is_unique[players.email]');
		$this->form_validation->set_rules('password', 'Password', 'trim|min_length[6]|required');

		if($this->form_validation->run() == FALSE) {
			$response = array('status' => false,
								'error' => $this->form_validation->error_array(),
								'message' => validation_errors());
			$this->output
			        ->set_status_header(412)
			        ->set_content_type('application/json', 'utf-8')
			        ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			        ->_display();
			exit;
		} else {
		    $msg = $this->apirest_m->addPlayer($_POST);
		    if ($msg){
			logs_message('Pomyślnie zarejestrowano użytkownika ' . $_POST['name'], $_SERVER['HTTP_USER_AGENT'], $_SERVER['REMOTE_ADDR']);
				$response = array('status' => 'OK');
				$this->output
				        ->set_status_header(201)
				        ->set_content_type('application/json', 'utf-8')
				        ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
				        ->_display();
				exit;
		    }
			$response = array('status' => 'Something goes wrong! Pleas try again later.');
			$this->output
			        ->set_status_header(500)
			        ->set_content_type('application/json', 'utf-8')
			        ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			        ->_display();
			exit;
		}
	}

	public function login_player() {

		header("Access-Control-Allow-Origin: *");
		$_POST = $this->security->xss_clean($_POST);
		$this->form_validation->set_rules('login', 'Login', 'trim|min_length[2]');
		$this->form_validation->set_rules('password', 'Password', 'trim|min_length[6]|required');

		if($this->form_validation->run() == FALSE) {
			$response = array('status' => false,
								'error' => $this->form_validation->error_array(),
								'login' => validation_errors());
			$this->output
			        ->set_status_header(412)
			        ->set_content_type('application/json', 'utf-8')
			        ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			        ->_display();
			exit;
		} else {
		    $msg = $this->apirest_m->loginPlayer($_POST['login'], $_POST['password']);
		    if ($msg == true){
			logs_message('Użytkownik ' . $_POST['login'] . ' zalogował się', $_SERVER['HTTP_USER_AGENT'], $_SERVER['REMOTE_ADDR']);
				$response = array('status' => 'OK',
									'message' => $msg,
									'login' => 'You can play now!');
				$this->output
				        ->set_status_header(201)
				        ->set_content_type('application/json', 'utf-8')
				        ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
				        ->_display();
				exit;
		    } else {
			logs_message('Błąd podczas logowania użytkownika ' . $_POST['login'], $_SERVER['HTTP_USER_AGENT'], $_SERVER['REMOTE_ADDR']);
				$response = array('status' => 'False',
									'login' => 'Wrong login or password or you account is not activated');
				$this->output
				        ->set_status_header(412)
				        ->set_content_type('application/json', 'utf-8')
				        ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
				        ->_display();
				exit;
		    }
		}
	}

	public function add_points() {

		header("Access-Control-Allow-Origin: *");
		$_POST = $this->security->xss_clean($_POST);
			$actual_points = $this->back_m->get_player_points('ranking', $_POST['id'])->points;	
		    $msg = $this->apirest_m->addPoints($_POST['id'], $actual_points, $_POST['points']);
		    if ($msg == true){
			logs_message('Dodano '.$_POST['points'].' punktów użytkownikowi o ID ' . $_POST['id'], $_SERVER['HTTP_USER_AGENT'], $_SERVER['REMOTE_ADDR']);
				$response = array('status' => 'OK',
									'message' => $msg);
				$this->output
				        ->set_status_header(201)
				        ->set_content_type('application/json', 'utf-8')
				        ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
				        ->_display();
				exit;
		    } else {
			logs_message('Niedodano punktów ' . $_POST['points'] . ' użytkownikowi o ID ' . $_POST['id'] . '(Błąd systemu)', $_SERVER['HTTP_USER_AGENT'], $_SERVER['REMOTE_ADDR']);
				$response = array('status' => 'False',
									'message' => 'Something goes wrong!');
				$this->output
				        ->set_status_header(500)
				        ->set_content_type('application/json', 'utf-8')
				        ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
				        ->_display();
				exit;
		    }
		}
}