<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Apirest_m extends CI_Model  
{
	function addPlayer($data) {
		$data = $this->security->xss_clean($data);
	    
	    $insert['name'] = strtolower($data['name']);
	    $insert['email'] = strtolower($data['email']);
	    $insert['password'] = hashPassword($data['password']);
        $insert['hero'] = strtolower($data['hero']);
	    $insert['secret_key'] = md5($insert['name'].'|'.$insert['email']);

	    if($this->db->insert('players', $insert)) {
            $ranking['player_id'] = $this->db->insert_id();
            $ranking['player_name'] = $insert['name'];
            $this->db->insert('ranking', $ranking);
			sendAuth($insert['email'], $insert['secret_key']);
	        return true;
	    } 
	    return false;
	}

    public function activePlayer($table, $data, $secret_key) {
        $data = $this->security->xss_clean($data);
        $this->db->where(['secret_key' => $secret_key]);
        $query = $this->db->update($table, $data);
        return $query;
    }

    public function loginPlayer($login, $password) {
        $data['players'] = $this->back_m->get_all('players');
        $login = strtolower($login);
		foreach($data['players'] as $check) {
			if(($login == $check->name || $login == $check->email) && (password_verify($password, $check->password)) && $check->auth == '1') {
                $ranking = $this->back_m->get_player_points('ranking', $check->id);
                $player['id'] = $check->id;
                $player['login'] = $check->name;
                $player['hero'] = $check->hero;
                $player['points'] = $ranking->points;
				return $player;
            }
        }
    }

    public function addPoints($id, $actual_points, $points) {
        $data = $this->security->xss_clean($points);
        $insert['points'] = $actual_points + $points;
        $this->db->where(['player_id' => $id]);
        $query = $this->db->update('ranking', $insert);
        return true;
    }
}