<!-- ########## START: LEFT PANEL ########## -->
    <div class="br-logo"><a href=""><span>[</span>VikingBattle<span>]</span></a></div>
    <div class="br-sideleft overflow-y-auto">
      <label class="sidebar-label pd-x-15 mg-t-20">Nawigacja</label>
      <div class="br-sideleft-menu">
        <a href="<?php echo base_url(); ?>panel/players" class="br-menu-link 
        <?php if($this->uri->segment(2) == 'players'){echo 'active';} ?>">
          <div class="br-menu-item">
            <i class="menu-item-icon icon fas fa-users tx-20"></i>
            <span class="menu-item-label">Lista graczy</span>
          </div><!-- menu-item -->
        </a><!-- br-menu-link -->
        <a href="<?php echo base_url(); ?>panel/ranking" class="br-menu-link 
        <?php if($this->uri->segment(2) == 'ranking'){echo 'active';} ?>">
          <div class="br-menu-item">
            <i class="menu-item-icon icon fas fa-list-ol tx-20"></i>
            <span class="menu-item-label">Ranking</span>
          </div><!-- menu-item -->
        </a><!-- br-menu-link -->
        <a href="<?php echo base_url(); ?>panel/logs" class="br-menu-link 
        <?php if($this->uri->segment(2) == 'logs'){echo 'active';} ?>">
          <div class="br-menu-item">
            <i class="menu-item-icon icon fas fa-list tx-20"></i>
            <span class="menu-item-label">Logi</span>
          </div><!-- menu-item -->
        </a><!-- br-menu-link -->
        <a href="<?php echo base_url(); ?>panel/info" class="br-menu-link 
        <?php if($this->uri->segment(2) == 'info'){echo 'active';} ?>">
          <div class="br-menu-item">
            <i class="menu-item-icon icon fas fa-info tx-20"></i>
            <span class="menu-item-label">Info</span>
          </div><!-- menu-item -->
        </a><!-- br-menu-link -->
      </div><!-- br-sideleft-menu -->
    </div><!-- br-sideleft -->
    <!-- ########## END: LEFT PANEL ########## -->

    <!-- ########## START: HEAD PANEL ########## -->
    <div class="br-header">
      <div class="br-header-left">
        <div class="navicon-left hidden-md-down"><a id="btnLeftMenu" href=""><i class="icon ion-navicon-round"></i></a></div>
        <div class="navicon-left hidden-lg-up"><a id="btnLeftMenuMobile" href=""><i class="icon ion-navicon-round"></i></a></div>
      </div><!-- br-header-left -->
      <div class="br-header-right">
        <nav class="nav">
          <div class="dropdown">
            <a href="" class="nav-link nav-link-profile" data-toggle="dropdown">
              <span class="logged-name hidden-md-down"><?php echo ucfirst($_SESSION['rola']); ?></span>
            </a>
            <div class="dropdown-menu dropdown-menu-header wd-200">
              <ul class="list-unstyled user-profile-nav">
                <li><a href="<?php echo base_url(); ?>panel/profile"><i class="icon ion-ios-person"></i> Edytuj profil</a></li>
                <li><a href="<?php echo base_url(); ?>panel/home/logout"><i class="icon ion-power"></i> Wyloguj się</a></li>
              </ul>
            </div><!-- dropdown-menu -->
          </div><!-- dropdown -->
        </nav>
      </div><!-- br-header-right -->
    </div><!-- br-header -->
    <!-- ########## END: HEAD PANEL ########## -->
