<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function logs_message($message, $browser, $ip) {
    $CI = &get_instance();
	$insert['message'] = $message;
	$insert['browser'] = $browser;
	$insert['ip'] = $ip;

	$query = $CI->back_m->insert('logs', $insert);
    return $query;
}