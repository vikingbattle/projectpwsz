<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function sendAuth($email, $secret_key) {
	require 'application/libraries/mailer/config.php';
    require 'application/libraries/mailer/functions.php';
    require 'application/libraries/mailer/PHPMailerAutoload.php';
    $CI = &get_instance();
    $_POST['base_url'] = base_url();
    $_POST['secret_key'] = $secret_key;
    $mail = new PHPMailer;
    $mail->isSMTP();
    $mail->Host = $cfg['smtp_host'];
    $mail->SMTPAuth = true;         
    $mail->SMTPOptions = array(
        'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        )
    );
    $mail->Username = $cfg['smtp_user'];
    $mail->Password = $cfg['smtp_pass'];
    $mail->Port = $cfg['smtp_port'];
    $mail->setFrom($cfg['smtp_user'], 'VikingsBattle - weryfikacja konta');
    $mail->AddBCC($email);
    $mail->isHTML(true);
    $mail->CharSet = 'UTF-8';
    $mail->Subject = 'VikingsBattle - weryfikacja konta';
    $mail->Body    = build_mail_body($_POST, 'auth_mail.php');
    if(!$mail->send()) {
        return $mail->ErrorInfo;
        exit;
    } else {
    	return true;
    }
}