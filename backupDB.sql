-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 66062.m.tld.pl
-- Czas generowania: 14 Sty 2020, 09:42
-- Wersja serwera: 5.7.21-20-log
-- Wersja PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `baza66062_projektdawid`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `contact_settings`
--

CREATE TABLE `contact_settings` (
  `id` int(11) NOT NULL,
  `company` text COLLATE utf8_polish_ci NOT NULL,
  `name` text COLLATE utf8_polish_ci NOT NULL,
  `map` text COLLATE utf8_polish_ci NOT NULL,
  `address` text COLLATE utf8_polish_ci NOT NULL,
  `city` text COLLATE utf8_polish_ci NOT NULL,
  `zip_code` text COLLATE utf8_polish_ci NOT NULL,
  `phone1` text COLLATE utf8_polish_ci NOT NULL,
  `phone2` text COLLATE utf8_polish_ci NOT NULL,
  `email1` text COLLATE utf8_polish_ci NOT NULL,
  `email2` text COLLATE utf8_polish_ci NOT NULL,
  `label1` text COLLATE utf8_polish_ci NOT NULL,
  `label2` text COLLATE utf8_polish_ci NOT NULL,
  `label3` text COLLATE utf8_polish_ci NOT NULL,
  `label4` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `contact_settings`
--

INSERT INTO `contact_settings` (`id`, `company`, `name`, `map`, `address`, `city`, `zip_code`, `phone1`, `phone2`, `email1`, `email2`, `label1`, `label2`, `label3`, `label4`) VALUES
(1, 'Ad Awards', '', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2499.672183750093!2d16.174381315758907!3d51.20669187958678!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x470f0d628b86c7af%3A0xe609f38f2e447933!2sFabryczna%2022%2C%2059-220%20Legnica!5e0!3m2!1spl!2spl!4v1568356930164!5m2!1spl!2spl', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `example`
--

CREATE TABLE `example` (
  `id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` text COLLATE utf8_polish_ci NOT NULL,
  `subtitle` text COLLATE utf8_polish_ci NOT NULL,
  `description` text COLLATE utf8_polish_ci NOT NULL,
  `photo` text COLLATE utf8_polish_ci NOT NULL,
  `alt` text COLLATE utf8_polish_ci NOT NULL,
  `name_photo_1` text COLLATE utf8_polish_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `example`
--

INSERT INTO `example` (`id`, `created`, `title`, `subtitle`, `description`, `photo`, `alt`, `name_photo_1`) VALUES
(21, '2019-09-13 07:54:18', 'SELECT username FROM users WHERE id = 0 OR 1=1', '', '<p><br></p>', '2019-10-17/granite-brick-path_,()_a_ving-LRA4J2F.jpg', '', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `info`
--

CREATE TABLE `info` (
  `id` int(11) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` text COLLATE utf8_polish_ci,
  `subtitle` text COLLATE utf8_polish_ci,
  `description` text COLLATE utf8_polish_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `info`
--

INSERT INTO `info` (`id`, `created`, `title`, `subtitle`, `description`) VALUES
(1, '2020-01-09 20:19:09', 'Przykładowe info', 'Podtytuł przykładowego info', '<p>Przykładowe info lorem ipsum i tak dalej...</p>'),
(2, '2020-01-09 20:19:09', 'Przykładowe info', 'Podtytuł przykładowego info', '<p>Przykładowe info lorem ipsum i tak dalej...</p>'),
(3, '2020-01-09 20:19:09', 'Przykładowe info', 'Podtytuł przykładowego info', '<p>Przykładowe info lorem ipsum i tak dalej...</p>'),
(4, '2020-01-12 21:23:24', 'Informacje o grze', 'Info', '<p>Projekt gry przeglądarkowej oferującej grę w trybie jednego oraz wielu graczy.</p>');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `message` text COLLATE utf8_polish_ci,
  `ip` text COLLATE utf8_polish_ci,
  `browser` text COLLATE utf8_polish_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `logs`
--

INSERT INTO `logs` (`id`, `created`, `message`, `ip`, `browser`) VALUES
(2, '2020-01-08 15:18:44', 'Użytkownik admin pomyślnie się zalogował', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36'),
(3, '2020-01-08 15:19:41', 'Użytkownik admin pomyślnie się zalogował', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36'),
(4, '2020-01-08 15:34:53', 'Błąd podczas logowania użytkownika nean2111122112', '217.99.162.47', 'PostmanRuntime/7.21.0'),
(5, '2020-01-08 15:35:26', 'Błąd podczas logowania użytkownika nean2111122112', '217.99.162.47', 'PostmanRuntime/7.21.0'),
(6, '2020-01-08 15:36:12', 'Użytkownik nean2 zalogował się', '217.99.162.47', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36'),
(7, '2020-01-08 15:40:13', 'Dodano punkty użytkownikowi o ID 42', '217.99.162.47', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36'),
(8, '2020-01-08 15:40:39', 'Pomyślnie zarejestrowano użytkownika dawid', '217.99.162.47', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36'),
(9, '2020-01-08 15:41:15', 'Użytkownik dawid zalogował się', '217.99.162.47', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36'),
(10, '2020-01-08 15:41:52', 'Użytkownik nean2 zalogował się', '217.99.162.47', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36'),
(11, '2020-01-08 15:42:14', 'Dodano punkty użytkownikowi o ID 42', '217.99.162.47', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36'),
(12, '2020-01-08 15:42:35', 'Dodano punkty użytkownikowi o ID 42', '217.99.162.47', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36'),
(13, '2020-01-09 20:17:32', 'Użytkownik admin pomyślnie się zalogował', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36'),
(14, '2020-01-10 16:56:19', 'Użytkownik Karol Jaskot zalogował się', '5.172.235.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0'),
(15, '2020-01-10 16:57:40', 'Dodano 40 punktów użytkownikowi o ID 44', '5.172.235.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0'),
(16, '2020-01-10 17:19:56', 'Pomyślnie zarejestrowano użytkownika login', '37.30.26.62', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36 OPR/64.0.3417.150'),
(17, '2020-01-10 17:20:02', 'Błąd podczas logowania użytkownika login', '37.30.26.62', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36 OPR/64.0.3417.150'),
(18, '2020-01-10 17:21:01', 'Błąd podczas logowania użytkownika login', '37.30.26.62', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36 OPR/64.0.3417.150'),
(19, '2020-01-10 17:21:03', 'Błąd podczas logowania użytkownika login', '37.30.26.62', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36 OPR/64.0.3417.150'),
(20, '2020-01-12 17:05:23', 'Użytkownik nean2 zalogował się', '217.99.215.40', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36'),
(21, '2020-01-12 17:26:12', 'Użytkownik nean2 zalogował się', '217.99.215.40', 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Mobile Safari/537.36'),
(22, '2020-01-12 17:32:18', 'Użytkownik nean2 zalogował się', '217.99.215.40', 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Mobile Safari/537.36'),
(23, '2020-01-12 17:32:33', 'Użytkownik nean2 zalogował się', '217.99.215.40', 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Mobile Safari/537.36'),
(24, '2020-01-12 17:32:44', 'Użytkownik nean2 zalogował się', '217.99.215.40', 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Mobile Safari/537.36'),
(25, '2020-01-12 17:34:12', 'Użytkownik nean2 zalogował się', '217.99.215.40', 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Mobile Safari/537.36'),
(26, '2020-01-12 19:04:25', 'Użytkownik Karol Jaskot zalogował się', '31.0.120.14', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0'),
(27, '2020-01-12 20:18:18', 'Użytkownik Karol Jaskot zalogował się', '31.0.120.14', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0'),
(28, '2020-01-12 21:06:27', 'Użytkownik admin pomyślnie się zalogował', '217.99.215.40', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36'),
(29, '2020-01-12 21:07:46', 'Użytkownik admin pomyślnie się zalogował', '31.0.120.14', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0'),
(30, '2020-01-12 21:09:09', 'Użytkownik Karol Jaskot zalogował się', '31.0.120.14', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0'),
(31, '2020-01-12 21:09:56', 'Użytkownik nean2 zalogował się', '217.99.215.40', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36'),
(32, '2020-01-12 21:12:12', 'Użytkownik Karol Jaskot zalogował się', '31.0.120.14', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0'),
(33, '2020-01-12 21:21:14', 'Użytkownik admin pomyślnie się zalogował', '31.0.120.14', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0'),
(34, '2020-01-13 10:01:47', 'Pomyślnie zarejestrowano użytkownika test', '178.235.146.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36'),
(35, '2020-01-13 10:01:58', 'Błąd podczas logowania użytkownika test', '178.235.146.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36'),
(36, '2020-01-13 10:02:07', 'Błąd podczas logowania użytkownika test@example.com', '178.235.146.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36'),
(37, '2020-01-13 10:02:57', 'Użytkownik test@example.com zalogował się', '178.235.146.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36'),
(38, '2020-01-13 10:05:16', 'Użytkownik test zalogował się', '178.235.146.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36'),
(39, '2020-01-13 20:25:02', 'Użytkownik admin pomyślnie się zalogował', '37.248.158.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0'),
(40, '2020-01-13 20:29:00', 'Użytkownik Karol Jaskot zalogował się', '37.248.158.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0'),
(41, '2020-01-13 20:31:19', 'Pomyślnie zarejestrowano użytkownika Kares', '37.248.158.10', 'Mozilla/5.0 (Linux; Android 7.0; LG-M320) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.116 Mobile Safari/537.36 OPR/55.2.2719.50740'),
(42, '2020-01-13 20:31:56', 'Błąd podczas logowania użytkownika Kares', '37.248.158.10', 'Mozilla/5.0 (Linux; Android 7.0; LG-M320) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.116 Mobile Safari/537.36 OPR/55.2.2719.50740'),
(43, '2020-01-13 20:32:02', 'Błąd podczas logowania użytkownika Kares', '37.248.158.10', 'Mozilla/5.0 (Linux; Android 7.0; LG-M320) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.116 Mobile Safari/537.36 OPR/55.2.2719.50740'),
(44, '2020-01-13 20:32:11', 'Użytkownik Kares zalogował się', '37.248.158.10', 'Mozilla/5.0 (Linux; Android 7.0; LG-M320) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.116 Mobile Safari/537.36 OPR/55.2.2719.50740'),
(45, '2020-01-13 20:33:37', 'Pomyślnie zarejestrowano użytkownika nick', '37.30.20.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0'),
(46, '2020-01-13 20:34:29', 'Dodano 1 punktów użytkownikowi o ID 44', '37.248.158.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0'),
(47, '2020-01-13 21:15:17', 'Błąd podczas logowania użytkownika login', '37.30.20.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0'),
(48, '2020-01-13 21:15:36', 'Użytkownik nean2 zalogował się', '217.99.215.40', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36'),
(49, '2020-01-13 21:18:16', 'Użytkownik nick zalogował się', '37.30.20.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36 OPR/64.0.3417.150'),
(50, '2020-01-13 21:19:09', 'Użytkownik nick zalogował się', '37.30.20.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0'),
(51, '2020-01-13 21:19:31', 'Użytkownik nick zalogował się', '37.30.20.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0'),
(52, '2020-01-13 21:20:06', 'Użytkownik nick zalogował się', '37.30.20.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0'),
(53, '2020-01-13 21:20:26', 'Użytkownik nick zalogował się', '37.30.20.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0'),
(54, '2020-01-13 21:21:00', 'Użytkownik nick zalogował się', '37.30.20.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0'),
(55, '2020-01-13 21:21:53', 'Użytkownik nick zalogował się', '37.30.20.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0'),
(56, '2020-01-13 21:22:09', 'Użytkownik nick zalogował się', '37.30.20.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0'),
(57, '2020-01-13 21:22:23', 'Użytkownik nick zalogował się', '37.30.20.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0'),
(58, '2020-01-13 21:24:45', 'Użytkownik nick zalogował się', '37.30.20.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0'),
(59, '2020-01-13 21:25:35', 'Użytkownik nick zalogował się', '37.30.20.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0'),
(60, '2020-01-13 21:25:53', 'Użytkownik nick zalogował się', '37.30.20.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0'),
(61, '2020-01-13 21:26:36', 'Użytkownik nick zalogował się', '37.30.20.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0'),
(62, '2020-01-13 21:30:03', 'Użytkownik nick zalogował się', '37.30.20.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `players`
--

CREATE TABLE `players` (
  `id` int(11) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `auth` int(11) NOT NULL,
  `name` text,
  `email` text,
  `hero` text NOT NULL,
  `password` text,
  `secret_key` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `players`
--

INSERT INTO `players` (`id`, `created`, `auth`, `name`, `email`, `hero`, `password`, `secret_key`) VALUES
(42, '2020-01-06 15:09:20', 1, 'nean2', 'nean12.bg@gmail.com', 'eliwood', '$2y$12$AYIfoSt.JmX3nBAav1/CkOv.cgzu/T5/7iV0b1h9rdb/oMFoPl43S', ''),
(44, '2020-01-06 16:51:24', 1, 'karol jaskot', 'karol.jaskot@wp.pl', 'marcus', '$2y$12$AJSujhEZ2fqTGfDWPxmRTuvH24doa.kK47xuyEIpEBOqo26LZvyW2', ''),
(45, '2020-01-08 15:40:39', 1, 'dawid', 'dawid.plociennik13@gmail.com', 'lyn', '$2y$12$sKyc0RdR2lJRy21OZe.9w.ZOVyUN1Pfef8goQMakmofocuBFZPZdC', ''),
(46, '2020-01-10 17:19:39', 0, 'login', 'kdabal1997@gmail.com', '', '$2y$12$07YGk0.mKDpaH0gQLCOB2Op.jKu/KudAK5Uy0KXL3Q71qlvpfRIB6', '393408a95b7dba41eb6c00caec229ff9'),
(47, '2020-01-13 10:01:47', 1, 'test', 'test@example.com', '', '$2y$12$355zgfgBiKbaU94cghjVpOqqiSTIBCzV8JO/xkBDgcoxpu0mv9Phy', 'b1f728b7bfb6292804d861db98ab2d03'),
(48, '2020-01-13 20:31:19', 1, 'kares', 'xxkaresxx@wp.pl', 'lyn', '$2y$12$1eltEkz8rUek1X3tjr0Usex3RgWGTp2zmzDsoCXv8mgYR4eqj/4f2', ''),
(49, '2020-01-13 20:33:37', 1, 'nick', 'sztygar19@onet.pl', '', '$2y$12$azbcILTyCxi7DStHevuxnuvq91BQpPAppLCvJFZzf7LMRgfAK3wPe', '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ranking`
--

CREATE TABLE `ranking` (
  `id` int(11) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `player_id` int(11) DEFAULT NULL,
  `player_name` text NOT NULL,
  `points` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `ranking`
--

INSERT INTO `ranking` (`id`, `created`, `player_id`, `player_name`, `points`) VALUES
(14, '2020-01-06 15:09:20', 42, 'nean2', 1681),
(16, '2020-01-06 16:51:24', 44, 'karol jaskot', 81),
(17, '2020-01-08 15:40:39', 45, 'dawid', 1),
(18, '2020-01-10 17:19:39', 46, 'login', 1),
(19, '2020-01-13 10:01:47', 47, 'test', 1),
(20, '2020-01-13 20:31:19', 48, 'kares', 1),
(21, '2020-01-13 20:33:37', 49, 'nick', 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `meta_title` text COLLATE utf8_polish_ci NOT NULL,
  `description` text COLLATE utf8_polish_ci NOT NULL,
  `keywords` text COLLATE utf8_polish_ci NOT NULL,
  `privace` text COLLATE utf8_polish_ci NOT NULL,
  `logo` text COLLATE utf8_polish_ci NOT NULL,
  `fb_link` text COLLATE utf8_polish_ci NOT NULL,
  `inst_link` text COLLATE utf8_polish_ci NOT NULL,
  `yt_link` text COLLATE utf8_polish_ci NOT NULL,
  `tw_link` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `settings`
--

INSERT INTO `settings` (`id`, `meta_title`, `description`, `keywords`, `privace`, `logo`, `fb_link`, `inst_link`, `yt_link`, `tw_link`) VALUES
(1, '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` int(11) NOT NULL,
  `login` text COLLATE utf8_polish_ci NOT NULL,
  `email` text COLLATE utf8_polish_ci NOT NULL,
  `password` text COLLATE utf8_polish_ci NOT NULL,
  `first_name` text COLLATE utf8_polish_ci NOT NULL,
  `last_name` text COLLATE utf8_polish_ci NOT NULL,
  `avatar` text COLLATE utf8_polish_ci NOT NULL,
  `rola` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id`, `created`, `active`, `login`, `email`, `password`, `first_name`, `last_name`, `avatar`, `rola`) VALUES
(1, '2019-04-10 13:48:15', 1, 'admin', 'dawid.plociennik13@gmail.com', '$2y$12$KctJz0aVFYzrBNXpQ2xvve8CPzf6BDVgv7MnLmjp/ri2sI1jOutK.', 'Dawid', 'Płóciennik', '', 'administrator');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `contact_settings`
--
ALTER TABLE `contact_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `example`
--
ALTER TABLE `example`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `info`
--
ALTER TABLE `info`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `players`
--
ALTER TABLE `players`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `ranking`
--
ALTER TABLE `ranking`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT dla tabel zrzutów
--

--
-- AUTO_INCREMENT dla tabeli `contact_settings`
--
ALTER TABLE `contact_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `example`
--
ALTER TABLE `example`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT dla tabeli `info`
--
ALTER TABLE `info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT dla tabeli `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT dla tabeli `players`
--
ALTER TABLE `players`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT dla tabeli `ranking`
--
ALTER TABLE `ranking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT dla tabeli `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
