﻿<?php require './fb-init.php'; require_once './google-init.php'; $google_url = $gClient->createAuthUrl();?>
<?php if(isset($_SESSION['access_token'])){
  header("Location:home.php");
} ?>


<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>VikingBattle</title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="css/mdb.min.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="css/style.css" rel="stylesheet">
  <script type="text/javascript" src="js/cookies.js"></script>
</head>

<body id="loadGame">
  
  <!-- Start your project here-->
  <div style="height: 100vh">
    <div class="flex-center flex-column">

      <img src="img/logo.png" height="200" width="300" />
      <h5 class="animated fadeIn mb-3">Projekt prostej gry turowej</h5>

      
      <button type="button" id="login" class="btn btn-elegant b_background" data-toggle="modal" data-target="#loginModal">Zaloguj</button>
      <button type="button" class="btn btn-elegant b_background" data-toggle="modal" data-target="#registerModal">Rejestracja</button>
      <button type="button" class="btn btn-elegant b_background" data-toggle="modal" data-target="#infoModal">Info</button>
      <a href="ranking.php"><button type="button" class="btn btn-elegant b_background">Ranking</button></a>
    </div>
  </div>
  <!--Info Modal -->
  <div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true" style="background-color: rgba(0, 0, 0, 0.6) !important;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Info</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?php // utworzenie uchwytu do pliku
            $plik = fopen('info.txt','r');

            // przypisanie zawartości do zmiennej
            $zawartosc = fread($plik, 8192);

            echo $zawartosc; ?>
      </div>
      
    </div>
  </div>
</div>
  <!--Register modal-->
  <div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="background-color: rgba(0, 0, 0, 0.6) !important;">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      
      <div class="modal-body p-0">
	  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span class="pr-2 pt-3" aria-hidden="true">&times;</span>
        </button>
        <!-- Default form register -->
    <div class="text-center border border-dark p-5" action="" method="post">
        <p class="bg-danger" id="registerDanger"></p>
        <p class="bg-success" id="registerSuccess"></p>
        <p class="h4 mb-4">Zarejestruj się!</p>

        <div class="form-row mb-4">
            <div class="col">
                <!-- First name -->
                <input type="text" id="name" class="form-control" name="name"placeholder="Twój nick">
            </div>

        </div>

        <!-- E-mail -->
        <input type="email" id="email" class="form-control mb-4" name="email"placeholder="Twój E-mail">

        <!-- Password -->
        <input type="password" id="password" class="form-control mb-4" placeholder="Podaj hasło" name="password" aria-describedby="defaultRegisterFormPasswordHelpBlock">
        <select id="hero" class="form-control mb-4" name="hero" placeholder="Hero">
          <option value="" disabled selected>Hero</option>
          <option value="hector">Hector</option>
          <option value="eliwood">Eliwood</option>
          <option value="lyn">Lyn</option>
          <option value="marcus">Marcus</option>
        </select>
        <small id="defaultRegisterFormPasswordHelpBlock" class="form-text text-muted mb-4">
            
        </small>

        

        <!-- Newsletter -->
        <div class="custom-control custom-checkbox">
            <input type="checkbox" class="custom-control-input" id="defaultRegisterFormNewsletter">
            <label class="custom-control-label" for="defaultRegisterFormNewsletter">Subkrybuj, aby być na bieżąco! </label>
        </div>

        <!-- Sign up button -->
        <button class="btn btn-elegant my-4 btn-block" action="" id="registerButton"type="submit"onclick="registerForm()" >Zarejestruj się!</button>

        <!-- Social register -->
        <p>lub :</p>

        <a href="<?php echo $login_url; ?>" class="mx-2 fb-continue-a" role="button">
          <button class="btn btn-block fb-continue d-flex justify-content-center align-items-center">
            <i class="fab fa-facebook-f white-text pr-3"></i>
            <div class="fb-text">Kontynuuj z Facebook</div>
          </button>
        </a>

        <a href="<?php echo $google_url; ?>" class="mx-2 fb-continue-a" role="button">
          <button class="btn btn-block g-continue d-flex justify-content-center align-items-center">
            <i class="fab fa-google-plus-square white-text pr-3"></i>
            <div class="fb-text">Kontynuuj z Google</div>
          </button>
        </a>

        <hr>

        <!-- Terms of service -->
        <p>
            Klikając
            <em>Zarejestruj się</em> akceptujesz nasze
            <a href="" target="_blank">warunki korzystania z gry internetowej</a>

    </div>
    <!-- Default form register -->
      </div>
      
    </div>
  </div>
</div>
  <!--Login modal-->
  <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="background-color: rgba(0, 0, 0, 0.6) !important;">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      
      <div class="modal-body p-0">
		<div id="container">
		<div class="">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span class="pr-2 pt-4" aria-hidden="true">&times;</span>
        </button>
		</div>
        <!-- Default form login -->
        <div class="text-center border border-dark p-5" action="" method="post">

            <p class="h4 mb-4">Zaloguj się!</p>
            <p id="validInfo" class="text-danger mb-4 rules"></p>

            <!-- Email -->
            <input type="text" name="login" id="loginLogin" class="form-control mb-4" placeholder="Login">

            <!-- Password -->
            <input type="password" name="passwordLogin" id="passwordLogin" class="form-control mb-4" placeholder="Twoje hasło">

            <div class="d-flex justify-content-around">
                <div>
                    <!-- Remember me -->
                    
                </div>
                <div>
                    <!-- Forgot password -->
                    <a href="">Zapomniałeś hasła?</a>
                </div>
            </div>

            <!-- Sign in button -->
            <button class="btn btn-elegant btn-block my-4" type="button" onclick="loginForm()">Zaloguj się!</button>

            <!-- Register -->
            <p>
                Nie założyłeś jeszcze konta?
                <a href=""data-dismiss="modal" data-toggle="modal" data-target="#registerModal">Rejestracja</a>
            </p>

            <!-- Social login -->
            <p>albo zaloguj się przez:</p>

            <a href="<?php echo $login_url; ?>" class="mx-2 fb-continue-a" role="button">
          <button class="btn btn-block fb-continue d-flex justify-content-center align-items-center">
            <i class="fab fa-facebook-f white-text pr-3"></i>
            <div class="fb-text">Kontynuuj z Facebook</div>
            
          </button>
        </a>

        <a href="<?php echo $google_url; ?>" class="mx-2 fb-continue-a" role="button">
          <button class="btn btn-block g-continue d-flex justify-content-center align-items-center">
            <i class="fab fa-google-plus-square white-text pr-3"></i>
            <div class="fb-text">Kontynuuj z Google</div>
          </button>
        </a>
            
        </div>
        <!-- Default form login -->
    </div>
      </div>
      
    </div>
  </div>
</div>
  <!-- Start your project here-->

  <!-- SCRIPTS -->
  <!-- JQuery -->
  <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="js/mdb.min.js"></script>
  <script type="text/javascript" src="js/index.js"></script>
  <script type="text/javascript" src="js/login.js"></script>

<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1703499953148371',
      cookie     : true,
      xfbml      : true,
      version    : 'v5.0'
    });
      
    
      
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

  
</script>
  
  <script type="text/javascript">
    

  </script>

</body>

</html>
