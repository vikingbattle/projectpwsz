﻿<?php require './fb-init.php'; ?>
<?php if(!isset($_SESSION['access_token'])){
	header("Location:index.php");
} ?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Zagraj</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="css/home.css" rel="stylesheet">
</head>
<body>
   
    <!-- Start your project here-->
    <div style="height: 100vh">
        <div class="flex-center flex-column">
            <img src="img/logo.png" height="200" width="300" />
            <h5 class="animated fadeIn mb-3">Projekt prostej gry turowej</h5>

            <a href="../Game/index.html"><button type="button" class="btn btn-elegant b_background">Graj</button></a>
            <a href="ranking.php"><button type="button" class="btn btn-elegant b_background">Ranking</button></a>
            <button type="button" class="btn btn-elegant b_background" data-toggle="modal" data-target="#infoModal">Info</button>
            
            <a href="logout.php" ><button type="button" class="btn btn-elegant b_background">Wyloguj</button></a>
        
        </div>
    </div>

	<!--Info Modal -->
  <div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true" style="background-color: rgba(0, 0, 0, 0.6) !important;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Info</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?php // utworzenie uchwytu do pliku
            $plik = fopen('info.txt','r');

            // przypisanie zawartości do zmiennej
            $zawartosc = fread($plik, 8192);

            echo $zawartosc; ?>
      </div>
      
    </div>
  </div>
</div>
    <!-- Start your project here-->
    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="js/mdb.min.js"></script>
</body>
</html>
