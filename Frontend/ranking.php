<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Ranking - MobileVikings</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
	<link href="css/ranking.css" rel="stylesheet">
	
</head>
<body onload="ranking()">
	<div class="tb">
			<table class="table">
			<thead class="thead-dark">
			<tr>
			  <th scope="col ">#</th>
			  <th scope="col ">Nick gracza</th>
			  <th scope="col ">Punkty</th>
      
      
			</tr>
		  </thead>
		  <tbody id="tbody">
			<!-- <tr>
			  <th scope="row">1</th>
			  <td>Mark</td>
			  <td>1245</td>
      
      
			</tr> -->
			
		  </tbody>
		</table>	
	</div>

	<div>
        <div class="flex-center flex-column">
            <a href="home.php"><button type="button" class="btn btn-elegant b_background">Powrót</button></a>
        </div>
    </div>
   
<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
<script type="text/javascript">
	function ranking() {
    

    $.ajax({  
         type: "post", 
         url:"https://projektdawidkamil.przedprojekt.com/dawid/Backend/panel/ranking/get_api", 
          
         cache: false,
         complete:function(json) {
          console.log(json); 
          var table='';
          var tableLength = json.responseJSON.length;
          for(var i=0 ; i<tableLength ; i++){
          		table += '<tr><th scope="row">'+(i+1)+'</th><td>'+json.responseJSON[i].player_name+'</td><td>'+json.responseJSON[i].points+'</td></tr>';
          	}
          document.getElementById("tbody").innerHTML = table;
         },
         success:function(json) {
          	// console.log(json);
          	
          	
          	
          }
         
    });
  } 
</script>
</body>
</html>
