function loginForm() {
    login = document.getElementById('loginLogin').value;
    password = document.getElementById('passwordLogin').value;
    $.ajax({  
         type: "post", 
         url:"https://projektdawidkamil.przedprojekt.com/dawid/Backend/panel/players/login_player", 
         data: {login:login, password:password}, 
         cache: false,
         complete:function(json) {
          console.log(json); 
          document.getElementById("validInfo").innerHTML = json.responseJSON.login;
         },
         success:function(json) {
          console.log(json.message.hero); 
          if(json.status == "OK") {
            console.log('You are logged');
            setCookie("id", json.message.id, 1);
            setCookie("login", login, 1);
            setCookie("hero", json.message.hero, 1);
            setCookie("points", json.message.points, 1);
            window.location.href = "../Game";
          }
         }
    });
  } 

  function registerForm() {
    name = document.getElementById('name').value;
    email = document.getElementById('email').value;
    password = document.getElementById('password').value;
    hero = document.getElementById('hero').value;
    $.ajax({  
         type: "post", 
         url:"https://projektdawidkamil.przedprojekt.com/dawid/Backend/panel/players/add_player?fbclid=IwAR0xuNyzWk8GvWbJOgGixPEuXggAel64Tp_cGownGkQM5aTGqcIHYemv8ps", 
         data: {name:name , email:email , password:password , hero:hero}, 
         cache: false,
         complete:function(json) {
          console.log(json);
          if(typeof json.responseJSON.message != 'undefined'){
          	document.getElementById("registerDanger").innerHTML = json.responseJSON.message;
          } 
          
         },
         success:function(json) {
          console.log(json.status); 
          if(json.status == "OK") {
            console.log('Created account');
            document.getElementById("registerSuccess").innerHTML = 'Account has been created succesfully!';
            
            
            $("html").load("game.html");
          }
         }
    });
  } 
  